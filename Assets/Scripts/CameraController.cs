﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {

    [Range(1, 100)]
    public float speed = 25f; // скорость перемещения камеры при приближении к границам экрана

    Camera mainCamera;

    const float borderDeadline = 0.1f; // граница в процентах, после которой начинается перемещение камеры
    float cameraSize = 5; // размер текущей камеры
    
    bool fixedCamera = true; // если истина - камера фиксируется на текущей позиции

    void Start()
    {
        mainCamera = GetComponent<Camera>();
        speed /= 10000; // позволяет адаптировать скорость к текущему масштабу
    }

    void Update()
    {
        CheckCameraPosition();
    }

    // Проверяет позицию "мыши" и при необходимости перемещает камеру в нужное направление
    void CheckCameraPosition()
    {
        if (!fixedCamera)
        {
            Vector2 moveSpeed = new Vector2();
            moveSpeed = new Vector2(GetMoveSpeedX(Input.mousePosition), GetMoveSpeedY(Input.mousePosition));

            transform.position += new Vector3(moveSpeed.x, moveSpeed.y, 0);
        }
    }

    // Скорость перемещения по х
    float GetMoveSpeedX(Vector3 mousePosition)
    {
        if (mousePosition.x + Screen.width * borderDeadline > Screen.width)
        {
            float deadLineStep = mousePosition.x - Screen.width * (1 - borderDeadline); // Определяет на сколько близко мышь находится к границе экрана
            return deadLineStep * speed;
        }
        else if (mousePosition.x < Screen.width * borderDeadline)
        {
            float deadLineStep = Screen.width * borderDeadline - mousePosition.x; // Определяет на сколько близко мышь находится к границе экрана
            return deadLineStep * -speed;
        }
        else
            return 0;
    }

    // Скорость перемещения по Y
    float GetMoveSpeedY(Vector3 mousePosition)
    {
        if (mousePosition.y + Screen.height * borderDeadline > Screen.height)
        {
            float deadLineStep = mousePosition.y - Screen.height * (1 - borderDeadline); // Определяет на сколько близко мышь находится к границе экрана
            return deadLineStep * speed;
        }
        else if (mousePosition.y < Screen.height * borderDeadline)
        {
            float deadLineStep = Screen.height * borderDeadline - mousePosition.y; // Определяет на сколько близко мышь находится к границе экрана
            return deadLineStep * -speed;
        }
        else
            return 0;
    }

    void LateUpdate()
    {
        CheckCameraSize();
    }

    // Проверить размер камеры и обновить при необходимости
    void CheckCameraSize()
    {
        if (mainCamera && mainCamera.orthographicSize != cameraSize)
            mainCamera.orthographicSize = cameraSize;
    }

    // Вызывается из слайдера, который отвечает за настройки зума на сцене
    public void ChangeZoomTo(float zoom)
    {
        cameraSize = zoom;
    }

    // Зафиксировать камеру
    public void FixCam()
    {
        fixedCamera = true;
    }

    // Отменить фиксацию камеры
    public void UnfixCam()
    {
        fixedCamera = false;
    }
}
