﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapEditor : MonoBehaviour {

    public GameObject ground, groundB, groundC, water; // префабы описывающие блоки

    private GameObject currentTileType; // префаб описывающий текущий блок
    private List<Tile> tiles = new List<Tile>(); // список всех блоков на сцене
    private float menuHeight; // Высота меню (сверху)

    void Start()
    {
        GameObject menuPanel = GameObject.Find("MenuPanel");
        if (menuPanel)
            menuHeight = menuPanel.GetComponent<RectTransform>().sizeDelta.y;
    
        currentTileType = ground;
    }

    void Update()
    {
        CheckHotKeys();
    }

    // Обработка "горячих клавиш"
    void CheckHotKeys()
    {
        // Клавиатура
        if (Input.GetKeyDown(KeyCode.Alpha1))
            currentTileType = ground;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            currentTileType = groundB;
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            currentTileType = groundC;
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            currentTileType = water;

        // "Мышь"
        if (Input.GetMouseButton(0))
            InstantiateNewTile();
        else if (Input.GetMouseButton(1))
            RemoveTile();
    }

    // Создание нового блока
    void InstantiateNewTile()
    {
        // Сверху находится меню, поэтому объекты там создаватся не должны
        if (Input.mousePosition.y + menuHeight > Screen.height)
            return;

        Vector3 mousePositionInWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(new Vector2(mousePositionInWorld.x, mousePositionInWorld.y), 0.1f);

        if (hitColliders.Length < 1) // Клетка свободна
        {
            GameObject newTile = (GameObject)Instantiate(currentTileType, mousePositionInWorld, Quaternion.identity);

            Tile tile = newTile.GetComponent<Tile>();
            if (tile)
                tiles.Add(tile);

            CheckAllTiles();
        }
    }

    // Удаление блока со сцены
    void RemoveTile(){

        Vector3 mousePositionInWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(new Vector2(mousePositionInWorld.x, mousePositionInWorld.y), 0.1f);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            tiles.Remove(hitColliders[i].gameObject.GetComponent<Tile>());
            Destroy(hitColliders[i].gameObject);
        }

        CheckAllTiles();
    }

    // Проверить все блоки на сцене на соответствие нужному типу
    void CheckAllTiles()
    {
        for (int i = 0; i < tiles.Count; i++)
            tiles[i].CheckTile();
    }

    // Задать новый текущий блок
    public void useThisTile(GameObject translateTile)
    {
        currentTileType = translateTile;
    }
}
