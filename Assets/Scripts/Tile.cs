﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    // Спрайты описывают различные состояния данного блока
    public Sprite fullGround, leftGround, middleGround, rightGround, center;
    public Sprite leftNess, rightNess;

    private SpriteRenderer spriteRenderer; // компонент SpriteRenderer текущего объекта

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        CheckTile();
    }

    // Задает блоку нужное состояние в зависимости от состояний блоков вокруг него
    public void CheckTile()
    {
        if (!spriteRenderer)
            spriteRenderer = GetComponent<SpriteRenderer>();

        if (!spriteRenderer)
            return; // если компонента нет, то и поменять спрайт невозможно

        // Узнаем, есть ли по соседству другие блоки
        Collider2D hitCollidersUp = Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y + 1), 0.1f);
        Collider2D hitCollidersDown = Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y - 1), 0.1f);
        Collider2D hitCollidersLeft = Physics2D.OverlapCircle(new Vector2(transform.position.x - 1, transform.position.y), 0.1f);
        Collider2D hitCollidersRight = Physics2D.OverlapCircle(new Vector2(transform.position.x + 1, transform.position.y), 0.1f);

        // Проверка типа блока
        if (CheckCenterGround(hitCollidersUp))
            return;
        if (CheckLeftNess(hitCollidersDown, hitCollidersLeft, hitCollidersRight))
            return;
        if (CheckRightNess(hitCollidersDown, hitCollidersLeft, hitCollidersRight))
            return;
        if (CheckMiddleGround(hitCollidersLeft, hitCollidersRight))
            return;
        if (CheckRightGround(hitCollidersLeft))
            return;
        if (CheckLeftGround(hitCollidersRight))
            return;
        if (CheckFullGround())
            return; 
    }

    bool CheckFullGround()
    {
        if (fullGround)
        {
            spriteRenderer.sprite = fullGround;
            return true ;
        }
        else
            return false;
    }

    bool CheckLeftGround(Collider2D hitCollidersRight)
    {
        if (leftGround && hitCollidersRight)
        {
            spriteRenderer.sprite = leftGround;
            return true;
        }
        else
            return false;
    }

    bool CheckMiddleGround(Collider2D hitCollidersLeft, Collider2D hitCollidersRight)
    {
        if (middleGround && hitCollidersLeft && hitCollidersRight)
        {
            spriteRenderer.sprite = middleGround;
            return true;
        }
        else
            return false;
    }

    bool CheckRightGround(Collider2D hitCollidersLeft)
    {
        if (rightGround && hitCollidersLeft)
        {
            spriteRenderer.sprite = rightGround;
            return true;
        }
        else
            return false;
    }

    bool CheckCenterGround(Collider2D hitCollidersUp)
    {
        if (center && hitCollidersUp && hitCollidersUp.gameObject.name == gameObject.name)
        {
            spriteRenderer.sprite = center;
            return true;
        }
        else
            return false;
    }

    bool CheckLeftNess(Collider2D hitCollidersDown, Collider2D hitCollidersLeft, Collider2D hitCollidersRight)
    {
        if (!hitCollidersDown || hitCollidersDown.gameObject.name != gameObject.name)
        {
            if (leftNess && !hitCollidersLeft && hitCollidersRight)
            {
                spriteRenderer.sprite = leftNess;
                return true;
            }
        }

        return false;
    }

    bool CheckRightNess(Collider2D hitCollidersDown, Collider2D hitCollidersLeft, Collider2D hitCollidersRight)
    {
        if (!hitCollidersDown || hitCollidersDown.gameObject.name != gameObject.name)
        {
            if (rightNess && hitCollidersLeft && !hitCollidersRight)
            {
                spriteRenderer.sprite = rightNess;
                return true;
            }
        }

        return false;
    }
}