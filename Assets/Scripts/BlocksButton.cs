﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlocksButton : MonoBehaviour {

    Button blockButton; // текущая кнопка
    Animator animator; // аниматор текущей кнопки

    void Start()
    {
        blockButton = GetComponent<Button>();
        blockButton.onClick.AddListener(() =>
        {
            ChangeBlockButtonState();
        });

        animator = transform.parent.gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        CheckHotKeys();
    }

    // Обработка "горячих клавиш"
    void CheckHotKeys()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ChangeBlockButtonState();
    }

    // Изменяет состояние кнопки блоков
    void ChangeBlockButtonState()
    {
        if (BlockShowed())
            animator.SetTrigger("HideBlocks");
        else
            animator.SetTrigger("ShowBlocks");
    }

    // Возвращает текущее состояние кнопки блоков
    bool BlockShowed()
    {
        if (animator.GetCurrentAnimatorClipInfo(0)[0].clip.name == "ShowBlocks")
            return true;
        else
            return false;
    }
}
