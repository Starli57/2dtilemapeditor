﻿using UnityEngine;
using System.Collections;

public class OnGrid : MonoBehaviour {

    void Start()
    {
        ReplaceOnGrid(); 
    }

    // Dыравнять объект по сетке
    void ReplaceOnGrid()
    {
        transform.position = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), 0);
    }
}
