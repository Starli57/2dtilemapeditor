﻿using UnityEngine;
using System.Collections;

public class EditorGrid : MonoBehaviour {

    public float height, width; // размер сетки
    
    private GameObject mainCameraObject; // объект с главной камерой
    private Camera mainCameraComponent; // компонент главной камеры

    private float mainCameraSize; // размер главной камеры

    void Start()
    {
        CheckComponents();
    }

    void OnDrawGizmos()
    {
        if (height < 1 || width < 1)
            return; // grid must have size not less than 1

        CheckComponents();
        UpdateCameraSize();
        DrawGrid();
    }

    // Проверяет определение компонентов
    void CheckComponents()
    {
        if (!mainCameraObject)
            mainCameraObject = GameObject.Find("Main Camera");

        if (!mainCameraComponent)
            mainCameraComponent = mainCameraObject.GetComponent<Camera>();
    }

    // Обновляет размер камеры и обновляет его
    void UpdateCameraSize()
    {
        if (mainCameraObject)
            mainCameraSize = mainCameraObject.GetComponent<Camera>().orthographicSize * 2;
        else
            mainCameraSize = 50;
    }

    // Отрисовка заданной сетки
    void DrawGrid()
    {
        int amountLines = Mathf.RoundToInt(mainCameraSize);
        float distanceFromCenterX = Mathf.Round(transform.position.x), distanceFromCenterY = Mathf.Round(transform.position.y);

        Gizmos.color = new Color32(255, 255, 255, 64);
        for (float i = -amountLines; i < amountLines; i++)
        {
            Gizmos.DrawLine(new Vector3(Mathf.Floor(i * width) + 0.5f + distanceFromCenterX, -mainCameraSize + distanceFromCenterY, 0.0f),
                            new Vector3(Mathf.Floor(i * width) + 0.5f + distanceFromCenterX, mainCameraSize + distanceFromCenterY, 0.0f));

            Gizmos.DrawLine(new Vector3(-mainCameraSize + distanceFromCenterX, Mathf.Floor(i * height) + 0.5f + distanceFromCenterY, 0.0f),
                            new Vector3(mainCameraSize + distanceFromCenterX, Mathf.Floor(i * height) + 0.5f + distanceFromCenterY, 0.0f));
        }
    }
}
